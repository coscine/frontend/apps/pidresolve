declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

declare module '@coscine/app-util';
declare module '@coscine/component-library';

declare module "*.png" {
  const value: any;
  export default value;
}
declare module '*.svg' {
  import Vue, {VueConstructor} from 'vue';
  const content: VueConstructor<Vue>;
  export default content;
}
