export default {
    back: 'Back',
    localeValue: 'En',
    help: 'Help',
    disclaimer: 'Disclaimer',
    imprint: 'Imprint',
    contact: 'Contact',
    otherLocale: 'De',

    pidInformationHeadline: 'PID Information:',

    pidInformationDescription: 'This PID belongs to data stored in a Coscine resource. The data referenced by this PID is part of a research project in Coscine',

    pid: 'Persistent Identifier (PID)',
    pidLabel: 'Persistent Identifier (PID):',
    pidHelp: 'This is the Persistent Identifier (PID) of the resource.',
    pidToClipboard: 'PID has been copied to clipboard.',

    contactHeadline: 'Contact PID Owner:',
    contactDescription: 'In most cases the referenced data is not publicly available. You can contact the owner of the data to ask for access. The owner of this resource will be contacted by Coscine. The information you give in the form will be relayed to the owner.',

    name: 'Your Name',
    nameLabel: 'Your Name:',
    nameHelp: 'This is a required field.',

    email: 'Your Email Address',
    emailLabel: 'Your Email Address:',
    emailHelp: 'This is a required field.',

    message: 'Your Message',
    messageLabel: 'Your Message:',
    messageHelp: 'This is a required field.',
    messageLimit: ' Characters',

    send: 'Send',
    receiveCopy: 'Send me a copy',

    toastMessageSuccessHeader: 'Message was sent successfully',
    toastMessageSuccessBody: 'Your message was sent to the resource owner. If you selected to be sent a copy, you will also receive the request via e-mail.',

    toastMessageFailureHeader: 'Failed to send message',
    toastMessageFailureBody: 'The message could not be sent. Please try again. If the problem persists contact servicedesk@itc.rwth-aachen.de',

    toastMessageNotValidHeader: 'Not a valid PID',
    toastMessageNotValidBody: 'The received PID is not valid.',

    toastMessageDeletedHeader: 'Project/Resource of PID deleted',
    toastMessageDeletedBody: 'The assigned Project/Resource of this PID has been deleted.',

    character: 'Character',
};
