export default {
    back: 'Zurück',
    localeValue: 'De',
    help: 'Hilfe',
    disclaimer: 'Datenschutz',
    imprint: 'Impressum',
    contact: 'Kontakt',
    otherLocale: 'En',

    pidInformationHeadline: 'PID Informationen:',

    pidInformationDescription: 'Diese PID gehört zu Daten in einer Coscine-Ressource. Die referenzierten Daten sind Teil eines Forschungsprojekts in Coscine',

    pid: 'Persistent Identifier (PID)',
    pidLabel: 'Persistent Identifier (PID):',
    pidHelp: 'Dies ist die PID einer Ressource.',
    pidToClipboard: 'PID in Zwischenablage kopiert.',

    contactHeadline: 'Kontakt zu PID-Besitzer aufnehmen:',
    contactDescription: 'In den meisten Fällen sind die referenzierten Daten nicht öffentlich zugänglich. Sie können sich daher an den Besitzer der Daten wenden und um Zugang bitten. Der Besitzer dieser Ressource wird dann von Coscine kontaktiert. Die Informationen, die Sie im Formular angeben, werden so an den Besitzer weitergeleitet.',

    name: 'Ihr Name',
    nameLabel: 'Ihr Name:',
    nameHelp: 'Pflichtfeld.',

    email: 'Ihre Email Adresse',
    emailLabel: 'Ihre Email Adresse:',
    emailHelp: 'Pflichtfeld.',

    message: 'Ihre Nachricht',
    messageLabel: 'Ihre Nachricht:',
    messageHelp: 'Pflichtfeld.',
    messageLimit: ' Zeichen',

    send: 'Senden',
    receiveCopy: 'Kopie an mich senden',

    toastMessageSuccessHeader: 'Nachricht wurde versendet',
    toastMessageSuccessBody: 'Ihre Nachricht wurde an den Ressourcenbesitzer gesendet. Wenn Sie "Kopie an mich senden" ausgewählt haben, empfangen Sie ebenfalls eine Kopie der Nachricht.',

    toastMessageFailureHeader: 'Fehler beim Senden',
    toastMessageFailureBody:  'Die Nachricht konnte nicht gesendet werden. Bitte versuchen Sie es erneut. Falls der Fehler wieder Auftritt, wenden Sie sich bitte an servicedesk@itc.rwth-aachen.de',

    toastMessageNotValidHeader: 'Keine valide PID',
    toastMessageNotValidBody: 'Die erhaltene PID ist nicht valide.',

    toastMessageDeletedHeader: 'Projekt/Ressource de PID gelöscht',
    toastMessageDeletedBody: 'Das/Die zugewiesene Projekt/Ressource dieser PID wurde gelöscht.',

    character: 'Zeichen',
};
